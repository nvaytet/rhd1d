#######################
         RHD1D         
 --------------------- 
 1D explicit radiation 
     hydrodynamics     
 --------------------- 
 Written by N. Vaytet  
 Niels Bohr Institute  
 2016, Copenhagen, DK  
#######################

Use the Makefile to compile the code under Unix/Linux:

user>$ make
gfortran -O3 -fdefault-real-8 -ffree-line-length-none -c rhd1d.f90 -o rhd1d.o
gfortran -O3 -fdefault-real-8 -ffree-line-length-none -c setup.f90 -o setup.o
gfortran -O3 -fdefault-real-8 -ffree-line-length-none rhd1d.o setup.o -o rhd1d
user>$

You can select the problem/test case to run by changing the 'choose_setup' variable in the 'setup.f90' file.


To plot the results:

user>$ python plot_rhd1d.py <output number>
