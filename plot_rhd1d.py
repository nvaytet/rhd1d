from pylab import *

if len(sys.argv) > 1:
    iout = int(sys.argv[1])
else:
    iout = 1


fig = matplotlib.pyplot.figure()
ratio = 0.7
sizex = 10.0
fig.set_size_inches(sizex,ratio*sizex)

run_id = "%05d" % iout

au = 1.495980e+13

data = loadtxt('output-'+run_id+'.txt')
solu = loadtxt('sedov1d-spherical_solution.txt')

ax1 = subplot(221)
ax2 = subplot(222)
ax3 = subplot(223)
ax4 = subplot(224)

# Sod tube
ax1.plot(data[:,0],data[:,1],color='k')
ax1.set_xlabel('Distance (cm)')
ax1.set_ylabel('Density (g/cm3)')

ax2.plot(data[:,0],data[:,2]/1.0e5,color='k')
ax2.set_xlabel('Distance (cm)')
ax2.set_ylabel('Velocity (km/s)')

ax3.plot(data[:,0],data[:,3],color='k')
ax3.set_xlabel('Distance (cm)')
ax3.set_ylabel('Pressure (g/cm/s2)')

ax4.plot(data[:,0],data[:,6],color='k')
ax4.plot(data[:,0],data[:,7],color='r')
ax4.set_xlabel('Distance (cm)')
ax4.set_ylabel('Temperature (K)')





## Sedov

#ax1.plot(data[:,0],data[:,1],color='k')
#ax1.plot(solu[:,0],solu[:,1],color='r')
#ax1.set_xlabel('Distance (cm)')
#ax1.set_ylabel('Density (g/cm3)')

#ax2.plot(data[:,0],data[:,2],color='k')
#ax2.plot(solu[:,0],solu[:,2],color='r')
#ax2.set_xlabel('Distance (cm)')
#ax2.set_ylabel('Velocity (km/s)')

#ax3.plot(data[:,0],data[:,3],color='k')
#ax3.plot(solu[:,0],solu[:,3],color='r')
#ax3.set_xlabel('Distance (cm)')
#ax3.set_ylabel('Pressure (g/cm/s2)')

#ax4.semilogy(data[:,0],data[:,6],color='k')
#ax4.semilogy(solu[:,0],solu[:,4],color='r')
#ax4.set_xlabel('Distance (cm)')
#ax4.set_ylabel('Temperature (K)')


## Collapse
#ax1.loglog(data[:,0]/au,data[:,1],color='k')
#ax1.set_xlabel('Distance (AU)')
#ax1.set_ylabel('Density (g/cm3)')

#ax2.semilogx(data[:,0]/au,data[:,2],color='k')
#ax2.set_xlabel('Distance (AU)')
#ax2.set_ylabel('Velocity (km/s)')

#ax3.loglog(data[:,0]/au,data[:,3],color='k')
#ax3.set_xlabel('Distance (AU)')
#ax3.set_ylabel('Pressure (g/cm/s2)')

#ax4.loglog(data[:,0]/au,data[:,6],color='k')
#ax4.loglog(data[:,0]/au,data[:,7],color='r')
#ax4.set_xlabel('Distance (AU)')
#ax4.set_ylabel('Temperature (K)')


fig.savefig('rhd1d.pdf',bbox_inches='tight')