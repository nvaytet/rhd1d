subroutine setup

    use variables

    implicit none

    real    :: d1,d2,u1,u2,p1,p2,t1,t2,dr,vol,ted
    integer :: i,n,k,j,irad

    choose_setup = 'radiative shock'
    
    select case(trim(adjustl(choose_setup)))
    
    case('radiative shock') ! Radiative shock ===============
    
        ! Physics to be included
        compute_hydro = .true.
        compute_radiation = .true.
        gravity = .false.
        
        ! Grid geometry
        geom = 1  ! 1=cartesian, 2=cylindrical, 3=spherical
        
        ! Write output every nout timesteps
        nout = 1000
        
        ! Constants
        gam   = 1.4
        gm1   = gam - 1.0
        mu    = 1.0
        c_red = 1.0e-03 ! Fraction of the speed of light
        
        ! Define conservative variables inside cells  
        d1 = 7.78e-10
        u1 = -6.0e+05
        t1 = 10.0
        p1 =  d1*kb*t1/(mu*mh)
        lbox = 1.0e11
        time_limit = 8.0e4

        ! Boundary conditions
        boundary_l = 2 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
        boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed

        dr = lbox / real(nx)

        ! cell positions
        do i = 0,nx+1

            xl(i) = real(i-1)*dr
            xr(i) = xl(i) + dr
         
        enddo
        
        call initialise_mesh

        do i = 0,nx+1
         
            ! Conservative variables
            dens_old(i) = d1
            momx_old(i) = d1*u1
            egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1
            erad_old(i) = ar*(t1**4)
            
        enddo

        ! Opacity: this is kappa*rho, NOT kappa
        opacity_params(1) = 3.0e-10
        opacity_params(2) = 0.0
        opacity_params(3) = 0.0
        opacity = opacity_params(1)
        
    case('sod tube') ! Sod tube ===================================
    
        ! Physics to be included
        compute_hydro = .true.
        compute_radiation = .false.
        gravity = .false.
        
        ! Grid geometry
        geom = 1  ! 1=cartesian, 2=cylindrical, 3=spherical
        
        ! Write output every nout timesteps
        nout = 10
        
        ! Constants
        gam = 1.4
        gm1 = gam - 1.0
        mu = 1.0
        
        ! Define conservative variables inside cells  
        d1 = 1.0
        d2 = 0.125
        u1 = 0.0
        u2 = 0.0
        p1 = 1.0
        p2 = 0.1
        lbox = 1.0
        time_limit = 0.2

        ! Boundary conditions
        boundary_l = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
        boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed

        dr = lbox / real(nx)

        ! cell positions
        do i = 0,nx+1

            xl(i) = real(i-1)*dr
            xr(i) = xl(i) + dr

        enddo

        call initialise_mesh

        do i = 0,nx+1
         
            !      x(i) = (real(i-1)+0.5)*dx

            if(xc(i) < 0.5*lbox)then

                ! Conservative variables
                dens_old(i) = d1
                momx_old(i) = d1*u1
                egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1

            else

                ! Conservative variables
                dens_old(i) = d2
                momx_old(i) = d2*u2
                egas_old(i) = 0.5*d2*u2*u2 + p2 / gm1

            endif
            
        enddo
        
    case('sedov') ! Sedov ===================================
    
        ! Physics to be included
        compute_hydro = .true.
        compute_radiation = .false.
        gravity = .false.
        
        ! Grid geometry
        geom = 3  ! 1=cartesian, 2=cylindrical, 3=spherical
        
        ! Write output every nout timesteps
        nout = 1000
        
        ! Constants
        gam = 1.66666666666667
        gm1 = gam - 1.0
        mu = 2.31
        
        ! Define conservative variables inside cells
        d1 = 1.0e-24
        u1 = 0.0
        p1 = 1.38065812e-13
        p2 = 1.0e+43
        lbox = 2.0e+16
        time_limit = 7.0e6

        ! Boundary conditions
        boundary_l = 2 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
        boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed

        dr = lbox / real(nx)

        ! cell positions
        do i = 0,nx+1

            xl(i) = real(i-1)*dr
            xr(i) = xl(i) + dr

        enddo

        call initialise_mesh

        do i = 0,nx+1
         
            ! Conservative variables
            dens_old(i) = d1
            momx_old(i) = d1*u1
            egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1
            
        enddo
        
        irad = 3
        vol  = ageom/xgeom*(xr(irad)**geom)
        ted  = p2 / vol
        do i = 1,irad
             egas_old(i) = ted
        enddo
        
    case('marshak') ! Marshak wave ===============
    
        ! Physics to be included
        compute_hydro = .false.
        compute_radiation = .true.
        gravity = .false.
        
        ! Grid geometry
        geom = 1  ! 1=cartesian, 2=cylindrical, 3=spherical
        
        ! Write output every nout timesteps
        nout = 1000
        
        ! Constants
        gam   = 1.6666666666667
        gm1   = gam - 1.0
        mu    = 1.25e8
        c_red = 1.0e-01 ! Fraction of the speed of light
        
        ! Define conservative variables inside cells  
        d1 = 1.0e-3
        u1 = 0.0
        t1 = 300.0
        t2 = 1000.0
        lbox = 12.0
        time_limit = 1.36e-07

        p1 =  d1*kb*t1/(mu*mh)
        
        ! Boundary conditions
        boundary_l = 4 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
        boundary_r = 4 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed

        dens_bc_l = d1
        momx_bc_l = d1*u1
        egas_bc_l = 0.5*d1*u1*u1 + p1 / gm1
        erad_bc_l = ar*(t2**4)
        
        dens_bc_r = d1
        momx_bc_r = d1*u1
        egas_bc_r = 0.5*d1*u1*u1 + p1 / gm1
        erad_bc_r = ar*(t1**4)
        
        dr = lbox / real(nx)

        ! cell positions
        do i = 0,nx+1

            xl(i) = real(i-1)*dr
            xr(i) = xl(i) + dr

        enddo

        call initialise_mesh

        do i = 0,nx+1
         
            ! Conservative variables
            dens_old(i) = d1
            momx_old(i) = d1*u1
            egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1
            erad_old(i) = ar*(t1**4)

        enddo

        ! Opacity: this is kappa*rho, NOT kappa
        opacity_params(1) = 1.0
        opacity_params(2) = 0.0
        opacity_params(3) = 0.0
        opacity = opacity_params(1)
        
    case('blastwave') ! Blastwave with radiation ===================================
    
        ! Physics to be included
        compute_hydro = .true.
        compute_radiation = .true.
        gravity = .false.
        
        ! Grid geometry
        geom = 3  ! 1=cartesian, 2=cylindrical, 3=spherical
        
        ! Write output every nout timesteps
        nout = 1000
        
        ! Constants
        gam   = 1.6666666666667
        gm1   = gam - 1.0
        mu    = 1.0
        c_red = 1.0e-03 ! Fraction of the speed of light
        
        ! Define conservative variables inside cells
        d1 = 5.0e-06
        u1 = 0.0e+00
        t1 = 1.0e+03
        t2 = 1.0e+07
        lbox = 1.0e+14
        time_limit = 1.0e+06

        p1 = d1*kb*t1/(mu*mh)
        p2 = d1*kb*t2/(mu*mh)
        
        ! Boundary conditions
        boundary_l = 2 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
        boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed

        dr = lbox / real(nx)

        ! cell positions
        do i = 0,nx+1

            xl(i) = real(i-1)*dr
            xr(i) = xl(i) + dr

        enddo

        call initialise_mesh

        do i = 0,nx+1
        
            ! Conservative variables
            dens_old(i) = d1
            momx_old(i) = d1*u1
            erad_old(i) = ar*(t1**4)
         
            if(xc(i) < 2.0e+12)then
                write(*,*) i,'inside bomb'

                egas_old(i) = p2 / gm1
    !             erad_old(i) = ar*(t2**4)

            else

                egas_old(i) = p1 / gm1
    !             erad_old(i) = ar*(t1**4)

            endif
                 
        enddo

        ! Opacity: this is kappa*rho, NOT kappa
        opacity_params(1) = 2.0e-10
        opacity_params(2) = 0.0
        opacity_params(3) = 0.0
        opacity = opacity_params(1) * (d1**opacity_params(2)) * (t1**opacity_params(3))
        
    case('collapse') ! Gravitational collapse ===================================
    
        ! Physics to be included
        compute_hydro = .false.
        compute_radiation = .true.
        gravity = .false.
        
        ! Grid geometry
        geom = 3  ! 1=cartesian, 2=cylindrical, 3=spherical
        
        ! Write output every nout timesteps
        nout = 1000
        
        ! Constants
        gam   = 1.66666666666667
        gm1   = gam - 1.0
        mu    = 2.31
        c_red = 1.0e-03 ! Fraction of the speed of light
        
        ! Define conservative variables inside cells
    !     mass = 1.0*msun
        lbox = 7.5e16
        d1 = 1.0*msun / (4.0*pi*(lbox**3)/3.0)
        u1 = 0.0e+00
        t1 = 10.0
        time_limit = 1.0e+15

        p1 = d1*kb*t1/(mu*mh)
    !     p2 = d1*kb*t2/(mu*mh)
        
        ! Boundary conditions
        boundary_l = 2 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
        boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed

    !     dr = lbox / real(nx)
        dr = 1.0
        xl(1) = 0.0
        ! cell positions
        do i = 2,nx+1

            xl(i) = xl(i-1) + dr
            dr = dr * 1.01
         
        enddo
        xl = xl * lbox/xl(nx+1)
        dr = xl(2)-xl(1)
        xl(0) = xl(1)-dr

        ! cell positions
        do i = 0,nx
            xr(i) = xl(i+1)
        enddo
        dr = xl(nx+1)-xl(nx)
        xr(nx+1) = xl(nx+1)+dr

        call initialise_mesh

        do i = 0,nx+1
        
            ! Conservative variables
            dens_old(i) = d1
            momx_old(i) = d1*u1
            egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1
            erad_old(i) = ar*(t1**4)
                 
        enddo

        ! Opacity: this is kappa*rho, NOT kappa
        opacity_params(1) = 1.0e-4
        opacity_params(2) = 1.0
        opacity_params(3) = 2.0
        opacity = opacity_params(1) * (d1**opacity_params(2)) * (t1**opacity_params(3))
        
    case default
    
        write(*,*) 'Error: setup failed! Case '//trim(adjustl(choose_setup))//' not found'
        stop
    
    end select
    
    dens_new = dens_old
    momx_new = momx_old
    egas_new = egas_old
    erad_new = erad_old
    
    return

end subroutine setup
