module variables
    implicit none
    integer, parameter      :: nvar = 4
    integer, parameter      :: nx = 256
    integer                 :: boundary_l,boundary_r
    integer                 :: fld_flux_limiter = 2
    integer                 :: geom = 1  ! 1=cartesian, 2=cylindrical, 3=spherical
    real   , parameter      :: c_red = 1.0e-04
    real                    :: time,dt,gam,gm1,time_limit,lbox,cfl,mu
    real                    :: large_number = 1.0e+20,small_number = 1.0e-20
    real                    :: dens_bc_l,dens_bc_r,momx_bc_l,momx_bc_r
    real                    :: egas_bc_l,egas_bc_r,erad_bc_l,erad_bc_r
    real                    :: c = 2.99792458e+10,kb = 1.38065812e-16
    real                    :: mh = 1.66053878e-24,ar=7.56591469e-15
    real                    :: pi = 3.14159265359,G=6.67259850e-08
    real                    :: msun = 1.989100e+33, lsun = 3.826800e+33
    real                    :: rsun = 6.959900e+10, pc = 3.085678e+18
    real                    :: au = 1.495980e+13
    real                    :: ageom,xgeom
    real, dimension(0:nx+1) :: xl,xc,xr,dx,dv,sl,sr,xt
    real, dimension(0:nx+1) :: opacity,u_face,prad,menc
    real, dimension(0:nx+1) :: dens_old,dens_new
    real, dimension(0:nx+1) :: momx_old,momx_new
    real, dimension(0:nx+1) :: egas_old,egas_new
    real, dimension(0:nx+1) :: erad_old,erad_new
    logical                 :: compute_hydro,compute_radiation,gravity
end module variables

!###############################################################################

program rhd1d

    use variables

    implicit none

    logical :: loop
    integer :: it,iout,nout

    write(*,'(a)') '########################'
    write(*,'(a)') '        RHD1D         '
    write(*,'(a)') '########################'
    write(*,*)

    ! Parameters (can be changed by the user)
    nout       = 1 ! Write output every nout timesteps
    cfl        = 0.3

    ! Other variables
    loop       = .true.
    iout       = 0
    it         = 0
    time       = 0.0
    compute_hydro = .true.
    compute_radiation = .false.
    gravity = .false.

    call setup

    call output(iout)

    do while(loop)

        it = it + 1

        call boundary_conditions

        call compute_timestep

        if(compute_hydro) call hydrodynamics

        if(compute_radiation) call radiative_transfer

        time = time + dt

        !      write(*,'(a,i9,a,es10.3,a,es10.3)') 'it = ',it,'  time = ',time,'  dt = ',dt

        if(mod(it,nout) == 0)then
            write(*,'(a,i9,a,es10.3,a,es10.3)') 'it = ',it,'  time = ',time,'  dt = ',dt
            iout = iout + 1
            call output(iout)
        endif

        if(time >= time_limit) loop = .false.

        !      read(*,*)

    enddo

    iout = iout + 1
    call output(iout)

    stop

end program rhd1d

!###############################################################################

subroutine setup

    use variables

    implicit none

    real    :: d1,d2,u1,u2,p1,p2,t1,t2,dr,vol,ted
    integer :: i,n,k,j,irad

    ! Constants
    gam = 1.4
    gm1 = gam - 1.0
    mu = 1.0
    
    if(geom == 1)then ! Cartesian
        xgeom = 1.0
        ageom = 1.0
    elseif(geom == 2)then ! Cylindrical
        xgeom = 2.0
        ageom = 2.0*pi
    elseif(geom == 3)then ! Spherical
        xgeom = 3.0
        ageom = 4.0*pi
    endif
    
    u_face = 0.0
    menc = 0.0
    
!     ! Radiative shock ===============
!     ! Define conservative variables inside cells  
!     d1 = 7.78e-10
!     u1 = -6.0e+05
!     t1 = 10.0
!     p1 =  d1*kb*t1/(mu*mh)
!     lbox = 1.0e11
!     time_limit = 8.0e4
! 
!     ! Boundary conditions
!     boundary_l = 2 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
!     boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
! 
!     dr = lbox / real(nx)
! 
!     ! cell positions
!     do i = 0,nx+1
! 
!         xl(i) = real(i-1)*dr + 0.1*lbox
!         xr(i) = xl(i) + dr
! 
!         dx(i) = xr(i) - xl(i)
!         xc(i) = (xgeom/(xgeom+1.0))*(xr(i)**(geom+1) - xl(i)**(geom+1)) / (xr(i)**geom - xl(i)**geom)
!         dv(i) = ageom/xgeom * (xr(i)**geom - xl(i)**geom)
!         sl(i) = ageom*(xl(i)**(geom-1))
!         sr(i) = ageom*(xr(i)**(geom-1))
!      
!     enddo
! 
!     do i = 0,nx+1
!      
!         ! Conservative variables
!         dens_old(i) = d1
!         momx_old(i) = d1*u1
!         egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1
!         erad_old(i) = ar*(t1**4)
! 
!         dens_new(i) = dens_old(i)
!         momx_new(i) = momx_old(i)
!         egas_new(i) = egas_old(i)
!         erad_new(i) = erad_old(i)
!         
!     enddo
! 
!     ! Opacity: this is kappa*rho, NOT kappa
!     opacity = 3.0e-10

    ! Sod tube ===================================
    ! Define conservative variables inside cells  
    d1 = 1.0
    d2 = 0.125
    u1 = 0.0
    u2 = 0.0
    p1 = 1.0
    p2 = 0.1
    lbox = 1.0
    time_limit = 0.2

    ! Boundary conditions
    boundary_l = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
    boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed

    dr = lbox / real(nx)

    ! cell positions
    do i = 0,nx+1

        xl(i) = real(i-1)*dr
        xr(i) = xl(i) + dr

        dx(i) = xr(i) - xl(i)
        xc(i) = (xgeom/(xgeom+1.0))*(xr(i)**(geom+1) - xl(i)**(geom+1)) / (xr(i)**geom - xl(i)**geom)
        if(geom == 3)then
            xt(i) = ((xgeom-1.0)/xgeom)*(xr(i)**(geom) - xl(i)**(geom)) / (xr(i)**(geom-1) - xl(i)**(geom-1))
        else
            xt(i) = 0.5*(xl(i)+xr(i))
        endif
        dv(i) = ageom/xgeom * (xr(i)**geom - xl(i)**geom)
        sl(i) = ageom*(xl(i)**(geom-1))
        sr(i) = ageom*(xr(i)**(geom-1))
     
    enddo

    do i = 0,nx+1
     
        !      x(i) = (real(i-1)+0.5)*dx

        if(xc(i) < 0.5*lbox)then

            ! Conservative variables
            dens_old(i) = d1
            momx_old(i) = d1*u1
            egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1

        else

            ! Conservative variables
            dens_old(i) = d2
            momx_old(i) = d2*u2
            egas_old(i) = 0.5*d2*u2*u2 + p2 / gm1

        endif

        dens_new(i) = dens_old(i)
        momx_new(i) = momx_old(i)
        egas_new(i) = egas_old(i)
        erad_new(i) = erad_old(i)
        
    enddo

!     ! Sedov ===================================
!     ! Define conservative variables inside cells
!       
!     d1 = 1.0e-24
!     u1 = 0.0
!     p1 = 1.38065812e-13
!     p2 = 1.0e+43
!     lbox = 2.0e+16
!     time_limit = 7.0e6
! 
!     gam = 1.66666666666667
!     gm1 = gam - 1.0
!     mu = 2.31
!     
!     ! Boundary conditions
!     boundary_l = 2 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
!     boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
! 
!     dr = lbox / real(nx)
! 
!     ! cell positions
!     do i = 0,nx+1
! 
!         xl(i) = real(i-1)*dr
!         xr(i) = xl(i) + dr
! 
!         dx(i) = xr(i) - xl(i)
!         xc(i) = (xgeom/(xgeom+1.0))*(xr(i)**(geom+1) - xl(i)**(geom+1)) / (xr(i)**geom - xl(i)**geom)
!         dv(i) = ageom/xgeom * (xr(i)**geom - xl(i)**geom)
!         sl(i) = ageom*(xl(i)**(geom-1))
!         sr(i) = ageom*(xr(i)**(geom-1))
!      
!     enddo
! 
!     do i = 0,nx+1
!      
!         ! Conservative variables
!         dens_old(i) = d1
!         momx_old(i) = d1*u1
!         egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1
!         
!     enddo
!     
!     irad = 3
!     vol  = ageom/xgeom*(xr(irad)**geom)
!     ted  = p2 / vol
!     do i = 1,irad
!          egas_old(i) = ted
!     enddo
! 
!     dens_new = dens_old
!     momx_new = momx_old
!     egas_new = egas_old
!     erad_new = erad_old
    
    
    
    
    
    
    
!     ! Marshak wave ===============
!     ! Define conservative variables inside cells  
!     d1 = 1.0e-3
!     u1 = 0.0
!     t1 = 300.0
!     t2 = 1000.0
!     lbox = 12.0
!     
!     gam = 1.6666666666667
!     gm1 = gam - 1.0
!     mu = 1.25e8
!     time_limit = 1.36e-07
! 
!     p1 =  d1*kb*t1/(mu*mh)
!     
!     ! Boundary conditions
!     boundary_l = 4 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
!     boundary_r = 4 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
! 
!     dens_bc_l = d1
!     momx_bc_l = d1*u1
!     egas_bc_l = 0.5*d1*u1*u1 + p1 / gm1
!     erad_bc_l = ar*(t2**4)
!     
!     dens_bc_r = d1
!     momx_bc_r = d1*u1
!     egas_bc_r = 0.5*d1*u1*u1 + p1 / gm1
!     erad_bc_r = ar*(t1**4)
!     
!     dr = lbox / real(nx)
! 
!     ! cell positions
!     do i = 0,nx+1
! 
!         xl(i) = real(i-1)*dr + 1.0
!         xr(i) = xl(i) + dr
! 
!         dx(i) = xr(i) - xl(i)
!         xc(i) = (xgeom/(xgeom+1.0))*(xr(i)**(geom+1) - xl(i)**(geom+1)) / (xr(i)**geom - xl(i)**geom)
!         dv(i) = ageom/xgeom * (xr(i)**geom - xl(i)**geom)
!         sl(i) = ageom*(xl(i)**(geom-1))
!         sr(i) = ageom*(xr(i)**(geom-1))
!      
!     enddo
! 
!     do i = 0,nx+1
!      
!         ! Conservative variables
!         dens_old(i) = d1
!         momx_old(i) = d1*u1
!         egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1
!         erad_old(i) = ar*(t1**4)
! 
!         dens_new(i) = dens_old(i)
!         momx_new(i) = momx_old(i)
!         egas_new(i) = egas_old(i)
!         erad_new(i) = erad_old(i)
!         
!     enddo
! 
!     ! Opacity: this is kappa*rho, NOT kappa
!     opacity = 1.0
    
    
    
!     ! Blastwave with radiation ===================================
!     ! Define conservative variables inside cells
!       
!     d1 = 5.0e-06
!     u1 = 0.0e+00
!     t1 = 1.0e+03
!     t2 = 1.0e+07
!     lbox = 1.0e+14
!     time_limit = 1.0e+06
! 
!     gam = 1.66666666666667
!     gm1 = gam - 1.0
!     mu = 1.0
!     
!     p1 = d1*kb*t1/(mu*mh)
!     p2 = d1*kb*t2/(mu*mh)
!     
!     ! Boundary conditions
!     boundary_l = 2 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
!     boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
! 
!     dr = lbox / real(nx)
! 
!     ! cell positions
!     do i = 0,nx+1
! 
!         xl(i) = real(i-1)*dr
!         xr(i) = xl(i) + dr
! 
!         dx(i) = xr(i) - xl(i)
!         xc(i) = (xgeom/(xgeom+1.0))*(xr(i)**(geom+1) - xl(i)**(geom+1)) / (xr(i)**geom - xl(i)**geom)
!         dv(i) = ageom/xgeom * (xr(i)**geom - xl(i)**geom)
!         sl(i) = ageom*(xl(i)**(geom-1))
!         sr(i) = ageom*(xr(i)**(geom-1))
!      
!     enddo
! 
!     do i = 0,nx+1
!     
!         ! Conservative variables
!         dens_old(i) = d1
!         momx_old(i) = d1*u1
!         erad_old(i) = ar*(t1**4)
!      
!         if(xc(i) < 2.0e+12)then
!             write(*,*) i,'inside bomb'
! 
!             egas_old(i) = p2 / gm1
! !             erad_old(i) = ar*(t2**4)
! 
!         else
! 
!             egas_old(i) = p1 / gm1
! !             erad_old(i) = ar*(t1**4)
! 
!         endif
!              
!     enddo
! 
!     dens_new = dens_old
!     momx_new = momx_old
!     egas_new = egas_old
!     erad_new = erad_old
!     
!     opacity = 2.0e-10
    
    
!     menc(1) = ageom/xgeom * (xc(i)**geom - xl(i)**geom) * dens_old(i)
!     do i = 2,nx+1
!         menc(i) = menc(i-1) + ageom/xgeom * (xr(i-1)**geom - xc(i-1)**geom) * dens_old(i-1)
!         menc(i) = menc(i  ) + ageom/xgeom * (xc(i  )**geom - xl(i  )**geom) * dens_old(i  )
!     enddo
    
    
!     ! Gravitational collapse ===================================
!     ! Define conservative variables inside cells
!     
! !     mass = 1.0*msun
!     lbox = 7.5e16
!     d1 = 1.0*msun / (4.0*pi*(lbox**3)/3.0)
!     u1 = 0.0e+00
!     t1 = 10.0
!     time_limit = 1.0e+15
! 
!     gam = 1.66666666666667
!     gm1 = gam - 1.0
!     mu = 2.31
!     
!     p1 = d1*kb*t1/(mu*mh)
! !     p2 = d1*kb*t2/(mu*mh)
!     
!     ! Boundary conditions
!     boundary_l = 2 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
!     boundary_r = 1 ! 1 = free-flow; 2 = reflexive; 3 = periodic; 4 = imposed
! 
! !     dr = lbox / real(nx)
!     dr = 1.0
!     xl(1) = 0.0
!     ! cell positions
!     do i = 2,nx+1
! 
!         xl(i) = xl(i-1) + dr
!         dr = dr * 1.01
!      
!     enddo
!     xl = xl * lbox/xl(nx+1)
!     dr = xl(2)-xl(1)
!     xl(0) = xl(1)-dr
! 
!     ! cell positions
!     do i = 0,nx
!         xr(i) = xl(i+1)
!     enddo
!     dr = xl(nx+1)-xl(nx)
!     xr(nx+1) = xl(nx+1)+dr
! 
!     do i = 0,nx+1
!         dx(i) = xr(i) - xl(i)
!         xc(i) = (xgeom/(xgeom+1.0))*(xr(i)**(geom+1) - xl(i)**(geom+1)) / (xr(i)**geom - xl(i)**geom)
!         xt(i) = ((xgeom-1.0)/xgeom)*(xr(i)**(geom) - xl(i)**(geom)) / (xr(i)**(geom-1) - xl(i)**(geom-1))
!         dv(i) = ageom/xgeom * (xr(i)**geom - xl(i)**geom)
!         sl(i) = ageom*(xl(i)**(geom-1))
!         sr(i) = ageom*(xr(i)**(geom-1))
!     enddo
! 
!     do i = 0,nx+1
!     
!         ! Conservative variables
!         dens_old(i) = d1
!         momx_old(i) = d1*u1
!         egas_old(i) = 0.5*d1*u1*u1 + p1 / gm1
!         erad_old(i) = ar*(t1**4)
!              
!     enddo
! 
!     dens_new = dens_old
!     momx_new = momx_old
!     egas_new = egas_old
!     erad_new = erad_old
!     
!     opacity = 1.0e-03*d1
    
    return

end subroutine setup

!###############################################################################

subroutine compute_timestep

    use variables

    implicit none

    integer :: i,j,k
    real    :: soundspeed,wavespeed,dt_hydro,dt_rad,kinetic_energy,internal_energy
    real    :: grad_er,lambda,rlim,kappa

    dt       = large_number
    dt_hydro = large_number
    dt_rad   = large_number

    if(gravity) menc(1) = ageom/xgeom * (xc(1)**geom - xl(1)**geom) * dens_old(1)
    
    do i = 1,nx
       
        if(compute_hydro)then
            kinetic_energy = 0.5 * momx_old(i) * momx_old(i)  / dens_old(i)
            internal_energy = egas_old(i) - kinetic_energy
            soundspeed = sqrt( gam * gm1 * internal_energy / dens_old(i) )
            wavespeed = abs(momx_old(i))/dens_old(i) + soundspeed
            dt_hydro = dx(i) / wavespeed
        endif

        if(compute_radiation)then
            call compute_opacity(dens_old(i),momx_old(i),egas_old(i),erad_old(i),kappa)
            opacity(i) = kappa
            grad_er = (erad_old(i+1) - erad_old(i-1)) / (xc(i+1) - xc(i-1))
            call lambda_fld(grad_er,opacity(i),erad_old(i),rlim,lambda)
            prad(i) = (lambda + lambda*lambda*rlim*rlim)*erad_old(i)
            dt_rad = dx(i)*dx(i) / (c*c_red*lambda/opacity(i))
        endif
!         write(*,*) dt_hydro,dt_rad

        dt = min(dt,dt_hydro,dt_rad)
        
        if(gravity)then
            menc(i) = menc(i-1) + ageom/xgeom * (xr(i-1)**geom - xc(i-1)**geom) * dens_old(i-1)
            menc(i) = menc(i  ) + ageom/xgeom * (xc(i  )**geom - xl(i  )**geom) * dens_old(i  )
!             write(*,*) i,menc(i)/msun
        endif
     
    enddo

    ! CFL condition
    dt = cfl * dt

    return

end subroutine compute_timestep

!###############################################################################

subroutine boundary_conditions

    use variables

    implicit none

    dens_old = dens_new
    momx_old = momx_new
    egas_old = egas_new
    erad_old = erad_new

    select case(boundary_l)
    case(1)
        dens_old(0) = dens_old(1)
        momx_old(0) = momx_old(1)
        egas_old(0) = egas_old(1)
        erad_old(0) = erad_old(1)
    case(2)
        dens_old(0) =  dens_old(1)
        momx_old(0) = -momx_old(1)
        egas_old(0) =  egas_old(1)
        erad_old(0) =  erad_old(1)
    case(3)
        dens_old(0) = dens_old(nx)
        momx_old(0) = momx_old(nx)
        egas_old(0) = egas_old(nx)
        erad_old(0) = erad_old(nx)
    case(4)
        dens_old(0) = dens_bc_l
        momx_old(0) = momx_bc_l
        egas_old(0) = egas_bc_l
        erad_old(0) = erad_bc_l
    end select

    select case(boundary_r)
    case(1)
        dens_old(nx+1) = dens_old(nx)
        momx_old(nx+1) = momx_old(nx)
        egas_old(nx+1) = egas_old(nx)
        erad_old(nx+1) = erad_old(nx)
    case(2)
        dens_old(nx+1) =  dens_old(nx)
        momx_old(nx+1) = -momx_old(nx)
        egas_old(nx+1) =  egas_old(nx)
        erad_old(nx+1) =  erad_old(nx)
    case(3)
        dens_old(nx+1) = dens_old(1)
        momx_old(nx+1) = momx_old(1)
        egas_old(nx+1) = egas_old(1)
        erad_old(nx+1) = erad_old(1)
    case(4)
        dens_old(nx+1) = dens_bc_r
        momx_old(nx+1) = momx_bc_r
        egas_old(nx+1) = egas_bc_r
        erad_old(nx+1) = erad_bc_r
    end select

    return
  
end subroutine boundary_conditions

!###############################################################################

subroutine hydrodynamics

    use variables

    implicit none

    integer            :: i,j
    real               :: dtdxl,dtdxr,source,rr
    real, dimension(5) :: var_l,var_r,flux

    do i = 1,nx+1

        ! Left state
        var_l(1) = dens_old(i-1)
        var_l(2) = momx_old(i-1)
        var_l(3) = egas_old(i-1)
        var_l(4) = erad_old(i-1)
        var_l(5) = opacity (i-1)*dx(i-1)

        ! Right state
        var_r(1) = dens_old(i  )
        var_r(2) = momx_old(i  )
        var_r(3) = egas_old(i  )
        var_r(4) = erad_old(i  )
        var_r(5) = opacity (i  )*dx(i  )

        ! Compute hydro fluxes with Riemann solver
        call riemann_solver(var_l,var_r,flux)

        ! Update cell values
        dtdxl = dt*sl(i  )/dv(i  )
        dtdxr = dt*sr(i-1)/dv(i-1)
!         if (i==1)then
!             write(*,*) sl(i  ),dv(i  ),sl(i  )/dv(i  ),sr(i-1),dv(i-1),sr(i-1)/dv(i-1)
!         endif
        dens_new(i  ) = dens_new(i  ) + flux(1)*dtdxl
        dens_new(i-1) = dens_new(i-1) - flux(1)*dtdxr
        momx_new(i  ) = momx_new(i  ) + flux(2)*dtdxl
        momx_new(i-1) = momx_new(i-1) - flux(2)*dtdxr
        egas_new(i  ) = egas_new(i  ) + flux(3)*dtdxl
        egas_new(i-1) = egas_new(i-1) - flux(3)*dtdxr
        erad_new(i  ) = erad_new(i  ) + flux(4)*dtdxl
        erad_new(i-1) = erad_new(i-1) - flux(4)*dtdxr

        ! Momentum geometry source
        source = ( var_r(3) - 0.5*var_r(2)*var_r(2)/var_r(1) ) * gm1
!         rr = (2.0/3.0)*(xr(i)**3-xl(i)**3)/(xr(i)**2-xl(i)**2)
        momx_new(i) = momx_new(i) + real(geom-1)*source*dt/xt(i)
        
        u_face(i) = flux(5)
        
        if(gravity)then
            source = -G*dens_old(i)*menc(i)/xc(i)**2
            momx_new(i) = momx_new(i) + source*dt
            egas_new(i) = egas_new(i) + source*dt*momx_old(i)/dens_old(i)
            
        endif
             
    enddo

    return

end subroutine hydrodynamics

!###############################################################################

subroutine radiative_transfer

    use variables

    implicit none

    integer :: i
    real    :: grad_er,er_mean,sig_mean,flux,tnew,pnew,fdtdx,lambda,rlim
    real    :: ke,pold,told,cv,source,source_derivative,omega,den

    do i = 1,nx+1

        ! Compute FLD flux on cell interface
        grad_er  = (erad_old(i) - erad_old(i-1)) / (xc(i) - xc(i-1))
        er_mean  = 0.5 * (erad_old(i-1) + erad_old(i))
        sig_mean = 0.5 * (opacity(i-1) + opacity(i))
        !      fdtdx    = - c * lambda_fld(grad_er,sig_mean,er_mean) * grad_er / sig_mean*dt/dx(i)
        call lambda_fld(grad_er,sig_mean,er_mean,rlim,lambda)
        fdtdx    = - c * lambda * grad_er / sig_mean*dt


        ! Update radiative energy
        erad_new(i  ) = erad_new(i  ) + c_red*fdtdx*sl(i  )/dv(i  )
        erad_new(i-1) = erad_new(i-1) - c_red*fdtdx*sr(i-1)/dv(i-1)

        ! Add radiation effects in momentum equation
        momx_new(i  ) = momx_new(i  ) - dens_new(i  )*opacity(i  )*fdtdx/c
        momx_new(i-1) = momx_new(i-1) + dens_new(i-1)*opacity(i-1)*fdtdx/c



      
    enddo

    ! Include radiation-matter coupling source term
    do i = 1,nx

        ke   = 0.5 * momx_new(i) * momx_new(i) / dens_new(i)
        pold = gm1 * (egas_new(i) - ke)
        told = mu*mh*pold/(kb*dens_new(i))
        cv   = dens_new(i)*kb/(mu*mh*gm1)

        source            =     ar*(told**4)
        source_derivative = 4.0*ar*(told**3)

        omega = opacity(i)*c*dt

        erad_new(i) = erad_new(i) + cv * omega*c_red * source / (cv + omega*c_red*source_derivative)
        erad_new(i) = erad_new(i) / (1.0+omega*c_red*cv/(cv + omega*c_red*source_derivative))

        tnew = (cv*told+omega*(erad_new(i)-source+source_derivative*told)) / (cv + omega*source_derivative)

        pnew = dens_new(i)*kb*tnew/(mu*mh)
        egas_new(i) = ke + pnew/gm1
        
        
        if(compute_hydro)then ! Add P.divU term for comoving frame

!             grad_er = (erad_old(i+1) - erad_old(i-1)) / (xc(i+1) - xc(i-1))
!             call lambda_fld(grad_er,opacity(i),erad_old(i),rlim,lambda)
!             pray = (lambda + lambda*lambda*rlim*rlim)*erad_old(i)
            erad_new(i) = erad_new(i) + prad(i) * (u_face(i+1)*sr(i)-u_face(i)*sl(i))*dt/dv(i)
            erad_new(i) = erad_new(i) + real(geom-1)*prad(i)*momx_old(i)/dens_old(i)*dt/xt(i)
            
        endif

    enddo

    return

end subroutine radiative_transfer

!###############################################################################

subroutine riemann_solver(ucl,ucr,flux)

    use variables

    implicit none

    real, dimension(5), intent(in ) :: ucl,ucr
    real, dimension(5), intent(out) :: flux

    real :: wl,wr
    real :: dl,pl,ul,el,ql,kl
    real :: dr,pr,ur,er,qr,kr
    real :: cfastl,rcl,cfastr,rcr
    real :: qstarl,qstarr
    real :: dstarl,dstarr
    real :: estarl,estarr
    real :: ustar,pstar
    real :: dd,uu,pp,ee,qq

    dl = ucl(1)
    ul = ucl(2) / ucl(1)
    el = ucl(3)
    pl = ( ucl(3) - 0.5*ucl(2)*ucl(2)/ucl(1) ) * gm1
    ql = ucl(4)
    kl = ucl(5)

    dr = ucr(1)
    ur = ucr(2) / ucr(1)
    er = ucr(3)
    pr = ( ucr(3) - 0.5*ucr(2)*ucr(2)/ucr(1) ) * gm1
    qr = ucr(4)
    kr = ucr(5)

    ! Find the largest eigenvalues in the normal direction to the interface
    cfastl = sqrt(gam*pl/dl + 4.0*ql*(1.0-exp(kl))/(9.0*dl))
    cfastr = sqrt(gam*pr/dr + 4.0*qr*(1.0-exp(kr))/(9.0*dr))
    !   cfastl = sqrt(gam*pl/dl)
    !   cfastr = sqrt(gam*pr/dr)

    ! Compute HLL wave speed
    wl = min(ul,ur) - max(cfastl,cfastr)
    wr = max(ul,ur) + max(cfastl,cfastr)

    ! Compute lagrangian sound speed
    rcl = dl * (ul - wl)
    rcr = dr * (wr - ur)

    ! Compute acoustic star state
    ustar = (rcr*ur   +rcl*ul   +  (pl-pr))/(rcr+rcl)
    pstar = (rcr*pl+rcl*pr+rcl*rcr*(ul-ur))/(rcr+rcl)

    ! Left star region variables
    dstarl = dl*(wl-ul)/(wl-ustar)
    estarl = ((wl-ul)*el-pl*ul+pstar*ustar)/(wl-ustar)
    qstarl = ql*(wl-ul)/(wl-ustar)

    ! Right star region variables
    dstarr = dr*(wr-ur)/(wr-ustar)
    estarr = ((wr-ur)*er-pr*ur+pstar*ustar)/(wr-ustar)
    qstarr = qr*(wr-ur)/(wr-ustar)

    ! Sample the solution at x/t=0
    if(wl>0.0)then
        dd = dl
        uu = ul
        pp = pl
        ee = el
        qq = ql
    elseif(ustar > 0.0)then
        dd = dstarl
        uu = ustar
        pp = pstar
        ee = estarl
        qq = qstarl
    elseif(wr > 0.0)then
        dd = dstarr
        uu = ustar
        pp = pstar
        ee = estarr
        qq = qstarr
    else
        dd = dr
        uu = ur
        pp = pr
        ee = er
        qq = qr
    end if

    ! Compute the Godunov flux
    flux(1) = dd*uu
    flux(2) = dd*uu*uu + pp
    flux(3) = (ee + pp) * uu
    flux(4) = qq*uu ! advection of radiative energy density
    flux(5) = uu

    return

end subroutine riemann_solver

!###############################################################################

subroutine lambda_fld(grad_er,sigma,er,rlim,lambda)

    use variables, only : small_number,c,fld_flux_limiter

    implicit none

    real, intent(in ) :: grad_er,sigma,er
    real, intent(out) :: rlim,lambda
    real              :: abs_grad_er

    abs_grad_er = abs(grad_er)

    if(abs_grad_er < small_number)then
        rlim = 0.0
    else
        rlim = abs_grad_er / (sigma * er)
    endif

    select case(fld_flux_limiter)
    case(1)
        lambda = 1.0 / 3.0
    case(2)
        if(rlim .lt. 1.0e-4)then
            lambda = 1.0/3.0
        else
            lambda = (1.0/tanh(rlim)-1.0/rlim) / rlim
        endif
    case(3)
        if(rlim .le. 1.5) then
            lambda = 2.0/(3.0+sqrt(9.0+12.0*rlim*rlim))
        else
            lambda = 1.0/(1.0 + rlim + sqrt(1.0+2.0*rlim))
        end if
    end select

    return

end subroutine lambda_fld

!###############################################################################

subroutine output(iout)

    use variables

    implicit none

    integer, intent(in) :: iout
    character (len=16)  :: fname
    integer             :: i,icol
    real :: d,u,p,tgas,e,er,trad
    
    write(fname,'(a,i5.5,a)') 'output-',iout,'.txt'

    open (21,file=fname,form='formatted')
    write(21,*) '# Time = ',time,'s : x    d    u    p    e    Er    Er    Tgas    Trad'
    do i = 1,nx
        d    = dens_new(i)
        u    = momx_new(i) / dens_new(i)
        e    = egas_new(i) - 0.5 * momx_new(i) * momx_new(i)  / dens_new(i)
        p    = gm1 * e
        tgas = mu*mh*p/(kb*d)
        er   = erad_new(i)
        trad = (er/ar)**0.25
        write(21,*) xc(i),d,u,p,e,er,tgas,trad
    enddo
    close(21)

    return

end subroutine output

!###############################################################################

subroutine compute_opacity(dens,momx,egas,erad,kappa)

    use variables

    implicit none

    real, intent(in ) :: dens,momx,egas,erad
    real, intent(out) :: kappa
    
    real :: ke,tgas,e,er,trad,p    
    
    ke = 0.5 * momx * momx / dens
    p  = gm1 * (egas - ke)
    tgas = mu*mh*p/(kb*dens)

    kappa = 1.0e-4*dens*(tgas**2)
    
    return

end subroutine compute_opacity
