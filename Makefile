#========================================================================================================
# Makefile for 1D exlicit RHD code RHD1D
#========================================================================================================
# 

# Name of executable file
#------------------------
EXE = rhd1d

# Compiler flavour
#-----------------
COMP = "GNU"
# COMP = "INTEL"

#========================================================================================================

# Fortran compiler & optimization options
#----------------------------------------

# GFORTRAN
ifeq ($(COMP),"GNU")
   F90 = gfortran
   FLAGS = -O3 -fdefault-real-8 -ffree-line-length-none
#    FLAGS = -cpp -fdefault-real-8 -ffree-line-length-none -fbounds-check -fbacktrace
#    FLAGS = -cpp -fdefault-real-8 -ffree-line-length-none -fcheck=all -fbacktrace -O0 -Wall -pedantic
#    FLAGS = -cpp -fdefault-real-8 -ffree-line-length-none -fbounds-check -fbacktrace -ffpe-trap=invalid,zero
#    FLAGS = -cpp -fdefault-real-8 -ffree-line-length-none -fcheck=all -fbacktrace -O0 -Wall -pedantic -ffpe-trap=invalid,zero,overflow,underflow
endif

# INTEL
ifeq ($(COMP),"INTEL")
   F90 = ifort
   FLAGS = -r8 -O3
#    FLAGS = -g -fpe0 -traceback -check all -check noarg_temp_created -debug extended -debug-parameters all
endif

#========================================================================================================

$(EXE):	rhd1d.o setup.o 
	$(F90) $(FLAGS) rhd1d.o setup.o -o $(EXE)

%.o:	%.f90
	$(F90) $(FLAGS) -c $< -o $@

#========================================================================================================
clean :
	rm -f *.o *.mod rhd1d output-*.txt
#========================================================================================================
